package chess;

public class BoardTester {
    public static void main(String[] args) {
        FindPathSolution soln = new FindPathSolution();
        if (args == null || args.length == 0){
            soln.solveProblem(5, 5, 0, 0, 3);
        }else{
            soln.solveProblem(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]));
        }
    }
}
