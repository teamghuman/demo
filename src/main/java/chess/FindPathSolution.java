package chess;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

class FindPathThread extends Thread {
    int[][] visited;
    int moveCount;
    int x;
    int y;
    int P;

    public FindPathThread(int[][] visited, int moveCount, int x, int y, int P){
        this.visited = visited;
        this.moveCount = moveCount;
        this.x = x;
        this.y = y;
        this.P = P;
    }

    public void run() {
        this.solveProblemUtil(this.visited, this.moveCount, this.x, this.y, this.P);
    }

    public void solveProblemUtil( int[][] visited, int moveCount, int x, int y, int P) {
        // Base Case : We were able to move to each square exactly once
        if(FindPathSolution.solution == P ) return;

        if (moveCount > FindPathSolution.X * FindPathSolution.Y ) {
            FindPathSolution.solution++;

            System.out.println(x + "," + y);
            if(FindPathSolution.finalPos.contains(new Position(x, y)))
                FindPathSolution.printSolution(visited);
        } else {
            for (int i = 0; i < FindPathSolution.xMoves.length; ++i) {
                int nextX = x + FindPathSolution.xMoves[i];
                int nextY = y + FindPathSolution.yMoves[i];
                // check if new position is a valid and not visited yet
                if ( FindPathSolution.isValidMove(visited, nextX, nextY) && visited[nextX][nextY] == 0) {
                    visited[nextX][nextY] = moveCount;
                    solveProblemUtil(visited, moveCount + 1, nextX, nextY, P);
                    visited[nextX][nextY] = 0;
               }
            }
        }

    }

}

class FindPathSolution extends Thread{
    public static final int[] xMoves = { 2, 1, -1, -2, -2, -1, 1, 2 };
    public static final int[] yMoves = { 1, 2, 2, 1, -1, -2, -2, -1 };
    public static int solution;
    public static int X ;
    public static int Y;
    public static Set<Position> finalPos = new HashSet<Position>();

    public void solveProblem(int X, int Y , int A,  int B, int P) {
        this.X = X;
        this.Y = Y;
        int[][] visited = new int[X][Y];
        for(int i = 0; i < 7 ; i++){
            finalPos.add(new Position(A+xMoves[i], B + yMoves[i]));
        }

        visited[A][B] = 1;
        // start knight's tour from top left corner square (0, 0)
        //solveProblemUtil(visited, 2, A, B, P);
        for(int i = 0 ; i < Math.min(Runtime.getRuntime().availableProcessors(), 7) ; i++){
            new FindPathThread(visited, 2, A+xMoves[i], B+yMoves[i], P).run();
        }


    }


    public static boolean isValidMove(int[][] visited, int x, int y) {
        if (x < 0 || x >= X || y < 0 || y >= Y) {
            return false;
        } else {
            return true;
        }
    }

    public static void printSolution(int[][] visited) {
        System.out.println("Solution " + solution);
        for (int i = 0; i < X ; i++) {
            for (int j = 0; j < Y ; j++) {
                System.out.print(visited[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }
}



class Position {
    int x;
    int y;
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return  "(" + x + "," + y + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x &&
                y == position.y;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}



